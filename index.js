const express = require("express");
const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.listen(port, () => console.log(`Server is running at localhost:${port}`))

app.get("/home", (req, res) => res.send("Welcome to the home page"))

app.get("/users", (req, res) => {
    let users = [
        {
            "username": "johndoe",
            "password": "johndoe1234"
        }
    ]
    for(let i = 0; i < users.length; i++){
        res.send(users[i])
    }
})

app.delete("/delete-user", (req, res) => {
    let users = [
        {
            "username": "pirateKing",
            "password": "luffy"
        },
        {
            "username": "johndoe",
            "password": "john123"
        }
    ]
    let message
    for(let i = 0; i < users.length; i++){
        if(req.body.username == users[i].username){
            users.splice(i, 1)
            message = `User ${req.body.username} has been deleted.`
            break;
        }
        else{
            message = `User does not exist!`
        }
    }
    res.send(message)
})